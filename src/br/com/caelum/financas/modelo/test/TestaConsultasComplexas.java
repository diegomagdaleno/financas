package br.com.caelum.financas.modelo.test;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.caelum.financas.dao.MovimentacaoDao;
import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.TipoMovimentacao;
import br.com.caelum.financas.util.JPAUtil;

public class TestaConsultasComplexas {
	
	public static void main(String[] args) {
		EntityManager manager = new JPAUtil().getEntityManager();
		exibeMediaNamedQuery(manager);
    }
	
	public static void exibeQuantidadeDeGastos(){
		EntityManager manager = new JPAUtil().getEntityManager();
		
		Conta conta = manager.find(Conta.class, 2);
		
		String jpql = "select count(m) from Movimentacao m where m.conta=:conta "
				+ "and m.tipoMovimentacao=:tipo";
		
		TypedQuery<Long> query = manager.createQuery(jpql, Long.class);
		
		query.setParameter("conta", conta);
		query.setParameter("tipo", TipoMovimentacao.SAIDA);
		
		Long count = query.getSingleResult();
		
		System.out.println("O quantidade de gastos da conta é " + count);
	}
	
	public static void exibeGastoMaximo(){
		EntityManager manager = new JPAUtil().getEntityManager();
		
		Conta conta = manager.find(Conta.class, 2);
		
		String jpql = "select max(m.valor) from Movimentacao m where m.conta=:conta "
				+ "and m.tipoMovimentacao=:tipo";
		
		TypedQuery<BigDecimal> query = manager.createQuery(jpql, BigDecimal.class);
		
		query.setParameter("conta", conta);
		query.setParameter("tipo", TipoMovimentacao.SAIDA);
		
		BigDecimal max = query.getSingleResult();
		
		System.out.println("O maior gasto da conta é " + max);
	}
	
	public static void exibeMediaPorDia(){
		EntityManager manager = new JPAUtil().getEntityManager();
		
		Conta conta = manager.find(Conta.class, 2);
		
		String jpql = "select avg(m.valor) from Movimentacao m where m.conta=:conta "
				+ "and m.tipoMovimentacao=:tipo "
				+ "group by m.data";
		
		TypedQuery<Double> query = manager.createQuery(jpql, Double.class);
		
		query.setParameter("conta", conta);
		query.setParameter("tipo", TipoMovimentacao.SAIDA);
		
		List<Double> medias = query.getResultList();
		
		for (int i=0; i<medias.size(); i++) {
			System.out.println("A média de gastos do dia " + (i+1) + " é: " + medias.get(i));
		}
	}
	
	public static void exibeMedia(EntityManager manager){
		MovimentacaoDao dao = new MovimentacaoDao(manager);
		
		Conta conta = manager.find(Conta.class, 2);
		
		Double media = dao.mediaDaContaPeloTipo(conta, TipoMovimentacao.SAIDA);
		
		System.out.println("A média de gastos da conta é " + media);
	}
	
	public static void exibeMediaNamedQuery(EntityManager manager){
		Conta conta = manager.find(Conta.class, 2);
		
		TypedQuery<Double> query = manager.
                createNamedQuery("mediaDaContaPeloTipoMovimentacao", Double.class);

        query.setParameter("pConta", conta);
        query.setParameter("pTipo", TipoMovimentacao.ENTRADA);

        Double media =  query.getSingleResult();
		
		System.out.println("A média de gastos da conta é " + media);
	}
	
	public static void exibeSoma(){
		EntityManager manager = new JPAUtil().getEntityManager();
		
		Conta conta = manager.find(Conta.class, 2);

		String jpql = "select sum(m.valor) from Movimentacao m where m.conta=:conta "
		        + "and m.tipoMovimentacao=:tipo";
		
		TypedQuery<BigDecimal> query = manager.createQuery(jpql, BigDecimal.class);
		
		query.setParameter("conta", conta);
		query.setParameter("tipo", TipoMovimentacao.SAIDA);
				
        BigDecimal soma = query.getSingleResult();
        
        System.out.println("A soma dos gastos da conta é " + soma);
	}
}
