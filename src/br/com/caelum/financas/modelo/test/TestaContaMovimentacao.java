package br.com.caelum.financas.modelo.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.util.JPAUtil;

public class TestaContaMovimentacao {
	public static void main(String[] args) {
		EntityManager manager = new JPAUtil().getEntityManager();

		String jpql = "select distinct c from Conta c left join fetch c.movimentacoes";
		TypedQuery<Conta> query = manager.createQuery(jpql, Conta.class);
        List<Conta> contas = query.getResultList();
        for(Conta c : contas) {
        	System.out.println("Titular: " + c.getTitular());
        	System.out.println("Número de movimentações: " + c.getMovimentacoes().size());
        }
    }
}
