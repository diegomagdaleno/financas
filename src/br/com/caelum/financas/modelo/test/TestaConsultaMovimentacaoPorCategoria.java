package br.com.caelum.financas.modelo.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.caelum.financas.modelo.Categoria;
import br.com.caelum.financas.modelo.Movimentacao;
import br.com.caelum.financas.util.JPAUtil;

public class TestaConsultaMovimentacaoPorCategoria {
	public static void main(String[] args) {

		EntityManager em = new JPAUtil().getEntityManager(JPAUtil.PERSISTENCE_UNIT_MYSQL);
        em.getTransaction().begin();

        Categoria categoria = new Categoria();
        categoria.setId(1);

        String jpql = "select m from Movimentacao m join m.categorias c where c=:categoria";
        TypedQuery<Movimentacao> query = em.createQuery(jpql, Movimentacao.class);
        query.setParameter("categoria", categoria);
        List<Movimentacao> movimentacoes = query.getResultList();

        for (Movimentacao m : movimentacoes) {
            System.out.println("\nDescricao ..: " + m.getDescricao());
            System.out.println("Valor ......: R$ " + m.getValor());
        }
        
        em.getTransaction().commit();    
        em.close();

    }
}
