package br.com.caelum.financas.modelo.test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;

import javax.persistence.EntityManager;

import br.com.caelum.financas.modelo.Categoria;
import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.Movimentacao;
import br.com.caelum.financas.modelo.TipoMovimentacao;
import br.com.caelum.financas.util.JPAUtil;

public class TestaMovimentacoesComCategoria {
	public static void main(String[] args) { 
		Categoria categoria1 = new Categoria("Viagem");
		Categoria categoria2 = new Categoria("Negócios");
		Categoria categoria3 = new Categoria("Lazer");
		
		Conta conta = new Conta();
        conta.setId(2); 
	
		Movimentacao movimentacao1 = new Movimentacao();
        movimentacao1.setData(Calendar.getInstance());
        movimentacao1.setDescricao("Viagem à SP");
        movimentacao1.setTipoMovimentacao(TipoMovimentacao.SAIDA);
        movimentacao1.setValor(new BigDecimal(150.0));
        movimentacao1.setCategorias(Arrays.asList(categoria1, categoria2));
        movimentacao1.setConta(conta);
        
        Movimentacao movimentacao2 = new Movimentacao();
        movimentacao2.setData(Calendar.getInstance());
        movimentacao2.setDescricao("Viagem à RJ");
        movimentacao2.setTipoMovimentacao(TipoMovimentacao.SAIDA);
        movimentacao2.setValor(new BigDecimal(400.0));
        movimentacao2.setCategorias(Arrays.asList(categoria1, categoria3));
        movimentacao2.setConta(conta);

        EntityManager em = new JPAUtil().getEntityManager(JPAUtil.PERSISTENCE_UNIT_MYSQL);
        em.getTransaction().begin();

        em.persist(categoria1);
        em.persist(categoria2);
        em.persist(categoria3);
        em.persist(movimentacao1);
        em.persist(movimentacao2);

        em.getTransaction().commit();    
        em.close();
	}
}
