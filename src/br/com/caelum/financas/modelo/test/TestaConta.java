package br.com.caelum.financas.modelo.test;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.EntityManager;

import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.Movimentacao;
import br.com.caelum.financas.modelo.TipoMovimentacao;
import br.com.caelum.financas.util.JPAUtil;

public class TestaConta {

	public static void main(String[] args) {
		
		cadastraMovimentacaoComContaTransient();
	}

	public static void cadastraMovimentacaoComContaTransient() {
		EntityManager manager = new JPAUtil().getEntityManager(JPAUtil.PERSISTENCE_UNIT_MYSQL);

		manager.getTransaction().begin();

		Conta conta = new Conta();

		// Movimentacoes da conta1
		Movimentacao movimentacao = new Movimentacao();

		movimentacao.setData(Calendar.getInstance());
		movimentacao.setDescricao("Conta de luz - ABRIL/2012");
		movimentacao.setValor(new BigDecimal("135"));
		movimentacao.setTipoMovimentacao(TipoMovimentacao.SAIDA);
		movimentacao.setConta(conta);

		manager.persist(movimentacao);

		manager.getTransaction().commit();

		manager.close();
	}

	public static void alteraManagedObjectAposCommit(){
		EntityManager em = new JPAUtil().getEntityManager(JPAUtil.PERSISTENCE_UNIT_MYSQL);
		
		em.getTransaction().begin();
		Conta conta = em.find(Conta.class, 1);
		em.getTransaction().commit();
		conta.setTitular("Outro titular");
		
		em.close();
	}
	
	public static void insereNovaConta() {
		Conta conta = new Conta();
		conta.setTitular("Novo titular");
		conta.setBanco("Itau");
		conta.setAgencia("0431");
		conta.setNumero("12054");
		
		EntityManager em = new JPAUtil().getEntityManager(JPAUtil.PERSISTENCE_UNIT_MYSQL);
		em.getTransaction().begin();
		em.persist(conta);
		em.getTransaction().commit();
		em.close();
	}
}

