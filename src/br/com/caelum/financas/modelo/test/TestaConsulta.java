package br.com.caelum.financas.modelo.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.Movimentacao;
import br.com.caelum.financas.modelo.TipoMovimentacao;
import br.com.caelum.financas.util.JPAUtil;

public class TestaConsulta {
    public static void main(String[] args) {
    	EntityManager em = new JPAUtil().getEntityManager(JPAUtil.PERSISTENCE_UNIT_MYSQL);
        em.getTransaction().begin();
        
        Conta conta = new Conta();
        conta.setId(2);
        
        String jpql = "select m from Movimentacao m where m.conta=:conta "
        		+ "and m.tipoMovimentacao=:tipo order by m.valor desc";
        TypedQuery<Movimentacao> query = em.createQuery(jpql, Movimentacao.class);
        query.setParameter("conta", conta);
        query.setParameter("tipo", TipoMovimentacao.ENTRADA);
        List<Movimentacao> movimentacoes = query.getResultList();

        for (Movimentacao movimentacao : movimentacoes) {
            System.out.println("\nDescricao ..: " + movimentacao.getDescricao());
            System.out.println("Valor ......: R$ " + movimentacao.getValor());
        }

        em.getTransaction().commit();    
        em.close();
    }
}
