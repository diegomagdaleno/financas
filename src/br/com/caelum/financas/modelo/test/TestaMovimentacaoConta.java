package br.com.caelum.financas.modelo.test;

import javax.persistence.EntityManager;

import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.Movimentacao;
import br.com.caelum.financas.util.JPAUtil;

public class TestaMovimentacaoConta {
	public static void main(String[] args) {
		EntityManager manager = new JPAUtil().getEntityManager();

        Movimentacao movimentacao = manager.find(Movimentacao.class, 2);

        System.out.println("Titular da conta: " + movimentacao.getConta().getTitular());
        
        Conta conta = movimentacao.getConta();
		System.out.println(conta.getMovimentacoes().size());
    }
}
