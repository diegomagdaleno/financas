package br.com.caelum.financas.util;

public class PopulaBaseDeDados {
	public static void main(String[] args) {
		PopulaConta.popula();
		PopulaMovimentacao.popula();
		PopulaCategoria.popula();
	}
}
