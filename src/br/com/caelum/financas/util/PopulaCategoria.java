package br.com.caelum.financas.util;

import javax.persistence.EntityManager;

import br.com.caelum.financas.modelo.Categoria;

public class PopulaCategoria {
	public static void popula(){
		Categoria categoria1 = new Categoria("Viagem");
		Categoria categoria2 = new Categoria("Negócios");
		Categoria categoria3 = new Categoria("Lazer");
		
        EntityManager em = new JPAUtil().getEntityManager(JPAUtil.PERSISTENCE_UNIT_MYSQL);
        em.getTransaction().begin();

        em.persist(categoria1);
        em.persist(categoria2);
        em.persist(categoria3);

        em.getTransaction().commit();    
        em.close();
	}
}
