package br.com.caelum.financas.util;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class JPAUtil {
	
	public static final String PERSISTENCE_UNIT_MYSQL = "financas-mysql";

	public EntityManager getEntityManager() {
		return getEntityManager(PERSISTENCE_UNIT_MYSQL);
	}
	
	public EntityManager getEntityManager(String persistenceUnit) {
		return Persistence.createEntityManagerFactory(persistenceUnit).createEntityManager();
	}

}
